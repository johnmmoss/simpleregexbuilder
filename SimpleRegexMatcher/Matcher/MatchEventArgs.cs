using System;
using System.Text.RegularExpressions;

namespace SimpleRegexMatcher
{
    public class MatchEventArgs : EventArgs
    {
        public Match Match { get; set; }
        public string Result { get; set; }
    }
}