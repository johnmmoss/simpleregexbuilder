using System;
using System.Text;
using System.Text.RegularExpressions;

namespace SimpleRegexMatcher
{
    public class Matcher
    {
        private bool cancelMatch;

        public delegate void MatchEventHandler(MatchEventArgs matchEventArgs);
        public delegate void ExceptionEventHandler(ExceptionEventArgs exceptionEventArgs);

        private readonly string inputText;
        private readonly string patternText;
        private readonly RegexOptions regexOptions;
        private int matchCount;

        public event EventHandler OnStartMatching;
        public event EventHandler OnEndMatching;
        public event MatchEventHandler OnMatch;
        public event ExceptionEventHandler OnException;

        public Matcher(string inputText, string patternText, RegexOptions regexOptions)
        {
            this.inputText = inputText;
            this.patternText = patternText;
            this.regexOptions = regexOptions;
        }

        public void MatchText()
        {
            try
            {
                matchCount = 0;
                cancelMatch = false;

                var regex = new Regex(patternText, regexOptions);

                if (OnStartMatching != null) OnStartMatching(new object(), EventArgs.Empty);

                foreach (Match match in regex.Matches(inputText))
                {
                    if (cancelMatch) break;

                    var output = new StringBuilder();

                    LoadMatch(output, match);
                    
                    if (OnMatch != null)
                    {
                        OnMatch(new MatchEventArgs() { Result = output.ToString() });
                    }
                }
            }
            catch(Exception ex)
            {
                if (OnException !=  null) OnException(new ExceptionEventArgs() {Exception = ex});
            }
            finally
            {
                if (OnEndMatching != null) OnEndMatching(new object(), EventArgs.Empty);
            }
        }

        private void LoadMatch(StringBuilder output, Match match)
        {
            int groupCount = 0;

            output.AppendFormat("\r\n -------- MATCH [{0}]-------- \r\n", matchCount);
            output.Append(match.Groups[0].Value + "\r\n");

            if (match.Groups.Count > 1)
            {
                output.Append(("\r\n"));
                foreach (Group Group in match.Groups)
                {
                    output.Append((string.Format("[{0}.] {1}\r\n", groupCount, Group.Value)));

                    groupCount++;
                }
                output.Append(("\r\n"));
            }
            matchCount++;
        }

        public void CancelMatchText()
        {
            cancelMatch = true;
        }
    }
}