﻿namespace SimpleRegexMatcher
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnMatch = new System.Windows.Forms.Button();
            this.tbxRegex = new System.Windows.Forms.RichTextBox();
            this.tbxSearchText = new System.Windows.Forms.RichTextBox();
            this.cbIgnoreWhiteSpace = new System.Windows.Forms.CheckBox();
            this.cbIgnoreCase = new System.Windows.Forms.CheckBox();
            this.cbMultiline = new System.Windows.Forms.CheckBox();
            this.cbSingleLine = new System.Windows.Forms.CheckBox();
            this.lblResults = new System.Windows.Forms.RichTextBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.lkbMsdnRef = new System.Windows.Forms.LinkLabel();
            this.gpbResults = new System.Windows.Forms.GroupBox();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.gpbResults.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnMatch
            // 
            this.btnMatch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMatch.Location = new System.Drawing.Point(13, 4);
            this.btnMatch.Name = "btnMatch";
            this.btnMatch.Size = new System.Drawing.Size(88, 23);
            this.btnMatch.TabIndex = 0;
            this.btnMatch.Text = "Match";
            this.btnMatch.UseVisualStyleBackColor = true;
            this.btnMatch.Click += new System.EventHandler(this.btnMatch_Click);
            // 
            // tbxRegex
            // 
            this.tbxRegex.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxRegex.BackColor = System.Drawing.Color.Honeydew;
            this.tbxRegex.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxRegex.Location = new System.Drawing.Point(12, 32);
            this.tbxRegex.Name = "tbxRegex";
            this.tbxRegex.Size = new System.Drawing.Size(665, 51);
            this.tbxRegex.TabIndex = 1;
            this.tbxRegex.Text = "";
            // 
            // tbxSearchText
            // 
            this.tbxSearchText.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbxSearchText.BackColor = System.Drawing.Color.Honeydew;
            this.tbxSearchText.DetectUrls = false;
            this.tbxSearchText.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbxSearchText.Location = new System.Drawing.Point(12, 115);
            this.tbxSearchText.Name = "tbxSearchText";
            this.tbxSearchText.Size = new System.Drawing.Size(665, 206);
            this.tbxSearchText.TabIndex = 2;
            this.tbxSearchText.Text = "";
            // 
            // cbIgnoreWhiteSpace
            // 
            this.cbIgnoreWhiteSpace.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.cbIgnoreWhiteSpace.AutoSize = true;
            this.cbIgnoreWhiteSpace.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbIgnoreWhiteSpace.Location = new System.Drawing.Point(307, 89);
            this.cbIgnoreWhiteSpace.Name = "cbIgnoreWhiteSpace";
            this.cbIgnoreWhiteSpace.Size = new System.Drawing.Size(140, 20);
            this.cbIgnoreWhiteSpace.TabIndex = 4;
            this.cbIgnoreWhiteSpace.Text = "Ignore Whitespace";
            this.cbIgnoreWhiteSpace.UseVisualStyleBackColor = true;
            // 
            // cbIgnoreCase
            // 
            this.cbIgnoreCase.AutoSize = true;
            this.cbIgnoreCase.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbIgnoreCase.Location = new System.Drawing.Point(201, 89);
            this.cbIgnoreCase.Name = "cbIgnoreCase";
            this.cbIgnoreCase.Size = new System.Drawing.Size(100, 20);
            this.cbIgnoreCase.TabIndex = 5;
            this.cbIgnoreCase.Text = "Ignore Case";
            this.cbIgnoreCase.UseVisualStyleBackColor = true;
            // 
            // cbMultiline
            // 
            this.cbMultiline.AutoSize = true;
            this.cbMultiline.Checked = true;
            this.cbMultiline.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbMultiline.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbMultiline.Location = new System.Drawing.Point(120, 89);
            this.cbMultiline.Name = "cbMultiline";
            this.cbMultiline.Size = new System.Drawing.Size(75, 20);
            this.cbMultiline.TabIndex = 6;
            this.cbMultiline.Text = "Multiline";
            this.cbMultiline.UseVisualStyleBackColor = true;
            // 
            // cbSingleLine
            // 
            this.cbSingleLine.AutoSize = true;
            this.cbSingleLine.Checked = true;
            this.cbSingleLine.CheckState = System.Windows.Forms.CheckState.Checked;
            this.cbSingleLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cbSingleLine.Location = new System.Drawing.Point(13, 89);
            this.cbSingleLine.Name = "cbSingleLine";
            this.cbSingleLine.Size = new System.Drawing.Size(93, 20);
            this.cbSingleLine.TabIndex = 7;
            this.cbSingleLine.Text = "Single Line";
            this.cbSingleLine.UseVisualStyleBackColor = true;
            // 
            // lblResults
            // 
            this.lblResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblResults.BackColor = System.Drawing.SystemColors.GradientInactiveCaption;
            this.lblResults.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.lblResults.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblResults.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.lblResults.HideSelection = false;
            this.lblResults.Location = new System.Drawing.Point(9, 19);
            this.lblResults.Name = "lblResults";
            this.lblResults.ReadOnly = true;
            this.lblResults.Size = new System.Drawing.Size(647, 453);
            this.lblResults.TabIndex = 3;
            this.lblResults.Text = "";
            // 
            // btnClear
            // 
            this.btnClear.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnClear.Location = new System.Drawing.Point(107, 4);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(88, 23);
            this.btnClear.TabIndex = 8;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lkbMsdnRef
            // 
            this.lkbMsdnRef.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lkbMsdnRef.AutoSize = true;
            this.lkbMsdnRef.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lkbMsdnRef.Location = new System.Drawing.Point(442, 7);
            this.lkbMsdnRef.Name = "lkbMsdnRef";
            this.lkbMsdnRef.Size = new System.Drawing.Size(235, 16);
            this.lkbMsdnRef.TabIndex = 9;
            this.lkbMsdnRef.TabStop = true;
            this.lkbMsdnRef.Text = "MSDN Regular Expression Reference";
            this.lkbMsdnRef.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lkbMsdnRef_LinkClicked);
            // 
            // gpbResults
            // 
            this.gpbResults.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gpbResults.Controls.Add(this.lblResults);
            this.gpbResults.Location = new System.Drawing.Point(12, 327);
            this.gpbResults.Name = "gpbResults";
            this.gpbResults.Size = new System.Drawing.Size(665, 478);
            this.gpbResults.TabIndex = 10;
            this.gpbResults.TabStop = false;
            this.gpbResults.Text = "Match Results";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lblStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 808);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(690, 22);
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "statusStrip";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 17);
            this.lblStatus.Text = "Ready";
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.AliceBlue;
            this.ClientSize = new System.Drawing.Size(690, 830);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.gpbResults);
            this.Controls.Add(this.lkbMsdnRef);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.cbSingleLine);
            this.Controls.Add(this.cbMultiline);
            this.Controls.Add(this.cbIgnoreCase);
            this.Controls.Add(this.cbIgnoreWhiteSpace);
            this.Controls.Add(this.tbxSearchText);
            this.Controls.Add(this.tbxRegex);
            this.Controls.Add(this.btnMatch);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Main";
            this.Text = "Simple .NET Regex Matcher";
            this.Load += new System.EventHandler(this.Main_Load);
            this.Enter += new System.EventHandler(this.RegexMatcherMain_Enter);
            this.gpbResults.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnMatch;
        private System.Windows.Forms.RichTextBox tbxRegex;
        private System.Windows.Forms.RichTextBox tbxSearchText;
        private System.Windows.Forms.CheckBox cbIgnoreWhiteSpace;
        private System.Windows.Forms.CheckBox cbIgnoreCase;
        private System.Windows.Forms.CheckBox cbMultiline;
        private System.Windows.Forms.CheckBox cbSingleLine;
        private System.Windows.Forms.RichTextBox lblResults;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.LinkLabel lkbMsdnRef;
        private System.Windows.Forms.GroupBox gpbResults;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
    }
}

