﻿using System;
using System.Drawing;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;

namespace SimpleRegexMatcher
{
    public partial class Main : Form
    {
        private Matcher matcher;
        private bool matching;

        public Main()
        {
            InitializeComponent();
        }
        //
        // Form/Control event handlers
        //
        private void Main_Load(object sender, EventArgs e)
        {
            tbxSearchText.Text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In neque orci, cursus ut elementum vitae, dictum nec enim. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Vestibulum a felis quis nulla ultricies dictum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Aliquam quis tortor id nisl pretium auctor pellentesque faucibus augue. Praesent ut neque ac quam aliquam lacinia a vel nisl. Morbi quis laoreet mauris. Vivamus cursus libero eu ligula aliquet hendrerit non a urna. Donec cursus, ipsum eget ornare dictum, mi velit facilisis turpis, ac varius lectus justo ut odio. Duis vestibulum libero ligula. Nunc ante orci, laoreet eget sollicitudin eget, sagittis sed purus. Sed convallis elementum ullamcorper. Phasellus quis orci nisl. Etiam sollicitudin nulla in lorem pharetra et pharetra justo ullamcorper. Maecenas aliquam urna nec orci consectetur ut tristique velit pellentesque.";
            tbxRegex.Text = @"\s\w+\s";
        }
        private void RegexMatcherMain_Enter(object sender, EventArgs e)
        {
            RunMatch();
        }
        private void btnMatch_Click(object sender, EventArgs e)
        {
            RunMatch();
        }
        private void btnClear_Click(object sender, EventArgs e)
        {
            lblResults.Text = string.Empty;
        }
        private void lkbMsdnRef_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            var url = System.Configuration.ConfigurationManager.AppSettings["MSDNReferenceLink"];

            if (string.IsNullOrEmpty(url)) return;

            lkbMsdnRef.LinkVisited = true;
            System.Diagnostics.Process.Start(url);
        }
        //
        // Matcher event handlers
        //
        private void matcher_OnStartMatching(object sender, EventArgs e)
        {
            EnableClearButton(false);
            
            MatchButtonText("Cancel", Color.Red);
            
            StatusLabelText("Matching...");
        }
        private void matcher_OnEndMatching(object sender, EventArgs e)
        {
            matching = false;

            EnableClearButton(true);
            MatchButtonText("Match", Color.Black);
            StatusLabelText("Ready");
        }
        private void matcher_OnMatch(MatchEventArgs matchEventArgs)
        {
            ResultsText(matchEventArgs.Result, false);
        }
        private void matcher_OnException(ExceptionEventArgs exceptionEventArgs)
        {
            MessageBox.Show(string.Format("There was a problem processing: {0}", exceptionEventArgs.Exception.Message), "There was a problem");
        }
        //
        // Cross thread control setters.
        //
        private delegate void ResultsTextDelegate(string text, bool warning);
        private void ResultsText(string text, bool warning)
        {
            if (lblResults.InvokeRequired)
            {
                lblResults.Invoke(new ResultsTextDelegate(ResultsText), new object[] { text, warning });
            }
            else
            {
                if (text == null)
                {
                    lblResults.Text = string.Empty;
                }
                else
                {
                    lblResults.AppendText(text);
                    lblResults.ForeColor = warning ? Color.Red : Color.Green;
                }
            }
        }

        private delegate void EnableClearButtonDelegate(bool enable);
        private void EnableClearButton(bool enable)
        {
            if (btnClear.InvokeRequired)
            {
                btnClear.Invoke(new EnableClearButtonDelegate(EnableClearButton), new object[] { enable });
            }
            else
            {
                btnClear.Enabled = enable;
            }
        }

        private delegate void MatchButtonTextDelegate(string text, Color color);
        private void MatchButtonText(string text, Color color)
        {
            if (btnClear.InvokeRequired)
            {
                btnClear.Invoke(new MatchButtonTextDelegate(MatchButtonText), new object[] { text, color });
            }
            else
            {
                btnMatch.Text = text;
                btnMatch.ForeColor = color;
            }
        }

        private delegate void StatusLabelTextDelegate(string text);
        private void StatusLabelText(string text)
        {
            if (btnClear.InvokeRequired)
            {
                btnClear.Invoke(new StatusLabelTextDelegate(StatusLabelText), new object[] { text });
            }
            else
            {
                lblStatus.Text = text;
            }
        }
        //
        // Private methods
        //
        private void RunMatch()
        {
            // Are we already running a match?
            if (matching)
            {
                matcher.CancelMatchText();
                matching = false;
                return;
            }

            // If not then check for valid inputs
            var regexText = tbxRegex.Text;
            var matchText = tbxSearchText.Text;

            if (string.IsNullOrEmpty(regexText))
            {
                ShowWarning("Regex Required", "Please provide a regex to search on.");
                return;
            }

            if (string.IsNullOrEmpty(matchText))
            {
                ShowWarning("Search Text Required", "Please provide some text to search.");
                return;
            }

            // Now we start a new match
            try
            {

                ResultsText(null, true);

                matcher = new Matcher(matchText, regexText, GetRegexOptions());
                matcher.OnStartMatching += matcher_OnStartMatching;
                matcher.OnEndMatching += matcher_OnEndMatching;
                matcher.OnMatch += matcher_OnMatch;
                matcher.OnException += matcher_OnException;

                matching = true;

                var thread = new Thread(matcher.MatchText);
                thread.IsBackground = true;
                thread.Start();
            }
            catch (Exception ex)
            {
                var caption = "There was an unexpected problem";
                var message = ex.Message;

                MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private static void ShowWarning(string caption, string message)
        {
            MessageBox.Show(message, caption, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        private RegexOptions GetRegexOptions()
        {
            var options = RegexOptions.None;

            if (cbSingleLine.Checked)
                options = options | RegexOptions.Singleline;

            if (cbMultiline.Checked)
                options = options | RegexOptions.Multiline;

            if (cbIgnoreCase.Checked)
                options = options | RegexOptions.IgnoreCase;

            if (cbIgnoreWhiteSpace.Checked)
                options = options | RegexOptions.IgnorePatternWhitespace;

            return options;
        }        
    }
}
